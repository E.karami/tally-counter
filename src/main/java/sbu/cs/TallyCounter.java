package sbu.cs;

public class TallyCounter implements TallyCounterInterface {
    private int value;
    @Override
    public void count() {
        if(this.value>9999){
            throw new IllegalValueException();
        }else if(this.value==9999){

        }else{
            this.value++;
        }
    }

    @Override
    public int getValue() {
        return this.value;
    }

    @Override
    public void setValue(int value) throws IllegalValueException {
        if(value<0||value>=10000){
            throw  new IllegalValueException();
        }else{
            this.value=value;
        }
    }
}
