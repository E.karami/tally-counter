package sbu.cs;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ExerciseLecture5 {

    /*
     *   implement a function to create a random password with
     *   given length using lower case letters
     *   lecture 5 page 14
     */
    public String weakPassword(int length) {
        StringBuilder result= new StringBuilder();
        for(int i=1;i<=length;i++){
            int randomNumber=(int)Math.floor(Math.random()*26)+97;
            result.append((char) randomNumber);
        }
        return result.toString();
    }

    /*
     *   implement a function to create a random password with
     *   given length and at least 1 digit and 1 special character
     *   lecture 5 page 14
     */
    public String strongPassword(int length) throws Exception {
        if(length<=2){
            throw new IllegalValueException();
        }
        StringBuilder result=new StringBuilder("");
        //for  conditions that each password must have a special ,digit and letter character
        result.append(subPassword(1,"special"));
        result.append(subPassword(1,"digit"));
        result.append(subPassword(1,"letter"));
        // updating password length
        length-=3;
        int specialLength=(int)Math.floor(Math.random()*(length+1));
        length-=specialLength;
        int letterLength=(int)Math.floor(Math.random()*(length+1));
        length-=letterLength;
        int digitLength=length;
        // filling remains length of password
        result.append(subPassword(specialLength,"special"));
        result.append(subPassword(digitLength,"digit"));
        result.append(subPassword(letterLength,"letter"));
        // randomize place of characters by shuffle method : fixing issues for new merge request ... result
        String[] strings=result.toString().split("");
        List<String> list=Arrays.asList(strings);
        Collections.shuffle(list);
        return String.join("", list.toArray(new String[0]));
    }

    /**
     * for special characters or letter characters or digit characters in strong password function
     * @param length input length of string
     * @return an string of random special characters or letter characters or digit chararcters
     */
    private String subPassword(int length ,String type) throws Exception{
        if(length<0){
            throw new Exception();
        }
        String characters="";
        if(type.equals("special")){
            characters=" !\"#$%&’()*+,-./:;<=>?@[\\]^_`{|}~";
        }else if(type.equals("letter")){
            characters="abcdefghijklmnopqrstouvwxyz";
        }else if(type.equals("digit")){
            characters="0123456789";
        }
        StringBuilder stringBuilder=new StringBuilder("");
        for(int i=1;i<=length;i++){
            int randomIndex=(int)Math.floor(Math.random()*characters.length());
            stringBuilder.append(characters.charAt(randomIndex));
        }
        return stringBuilder.toString();
    }

    /*
     *   implement a function that checks if a integer is a fibobin number
     *   integer n is fibobin is there exist an i where:
     *       n = fib(i) + bin(fib(i))
     *   where fib(i) is the ith fibonacci number and bin(i) is the number
     *   of ones in binary format
     *   lecture 5 page 17
     */
    public boolean isFiboBin(int n) {
        for(int i=1;;i++){
            int fibonacci=fibonacci(i);
            if(fibonacci>=n){
                return false;
            }
            if(n==fibonacci+binary(fibonacci)){
                return true;
            }
        }
    }

    /**
     * this method extracted for fibo in fiboBin function
     * @param n index of fibonacci number
     * @return fibonacci number
     */
    private int fibonacci(int n) {
        if(n>2){
            return fibonacci(n-1)+fibonacci(n-2);
        }else{
            return 1;
        }
    }

    /**
     * this method extracted for bin in fiboBin function
     * @param number input number
     * @return num of ones in binary format
     */
    private int binary(int number){
        int result=0;
        while (number!=0){
            if(number%2==1){
                result++;
            }
            number/=2;
        }
        return result;
    }
}
