package sbu.cs;

public class ExerciseLecture4 {

    /*
     *   implement a function that returns factorial of given n
     *   lecture 4 page 15
     */
    public long factorial(int n) {
        long result=1;
        for(int counter=1;counter<=n;counter++){
            result*=counter;
        }
        return result;
    }

    /*
     *   implement a function that return nth number of fibonacci series
     *   the series -> 1, 1, 2, 3, 5, 8, ...
     *   lecture 4 page 19
     */
    public long fibonacci(int n) {
        if(n>2){
            return fibonacci(n-1)+fibonacci(n-2);
        }else{
            return 1;
        }
    }

    /*
     *   implement a function that return reverse of a given word
     *   lecture 4 page 19
     */
    public String reverse(String word) {
        return new StringBuilder(word).reverse().toString();
    }

    /*
     *   implement a function that returns true if the given line is
     *   palindrome and false if it is not palindrome.
     *   palindrome is like 'wow', 'never odd or even', 'Wow'
     *   lecture 4 page 19
     */
    public boolean isPalindrome(String line) {
        String reverse=new StringBuilder(line).reverse().toString().replaceAll("\\s","");
        return line.replaceAll("\\s","").equalsIgnoreCase(reverse);
    }

    /*
     *   implement a function which computes the dot plot of 2 given
     *   string. dot plot of hello and ali is:
     *       h e l l o
     *   h   *
     *   e     *
     *   l       * *
     *   l       * *
     *   o           *
     *   lecture 4 page 26
     */
    public char[][] dotPlot(String str1, String str2) {
        char[][] result=new char[str1.length()][str2.length()];
        for (int i=0;i<str1.length();i++){
            for (int j=0;j<str2.length();j++){
                if(str1.charAt(i)==str2.charAt(j)){
                    result[i][j]='*';
                }else{
                    result[i][j]=' ';
                }
            }
        }
        return result;
    }
}
