package sbu.cs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Collections.*;

public class ExerciseLecture6 {

    /*
     *   implement a function that takes an array of int and return sum of
     *   elements at even positions
     *   lecture 6 page  16
     */
    public long calculateEvenSum(int[] arr) {
        long result =0;
        for(int i=0;i<arr.length;i+=2){
            result+=arr[i];
        }
        return result;
    }

    /*
     *   implement a function that takes an array of int and return that
     *   array in reverse order
     *   lecture 6 page 16
     */
    public int[] reverseArray(int[] arr) {
        List<Integer> list= Arrays.stream(arr).boxed().collect(Collectors.toList());
        reverse(list);
        return list.stream().mapToInt(i->i).toArray();
    }

    /*
     *   implement a function that calculate product of two 2-dim matrices
     *   lecture 6 page 21
     */
    public double[][] matrixProduct(double[][] m1, double[][] m2) throws RuntimeException {
        int length= m1[0].length;
        for (double[] doubles : m1) {
            if (doubles.length != length) {
                throw new RuntimeException();
            }
        }
        length= m2[0].length;
        for (double[] doubles : m2) {
            if (doubles.length != length) {
                throw new RuntimeException();
            }
        }
        if(m1[0].length!=m2.length){
            throw new RuntimeException();
        }
        double[][] result=new double[m1.length][m2[0].length];
        for(int i=0;i< m1.length;i++){
            for(int j=0;j<m2[0].length;j++){
                for(int k=0;k<m1[0].length;k++){
                    result[i][j]+=m1[i][k]*m2[k][j];
                }
            }
        }
        return result;
    }

    /*
     *   implement a function that return array list of array list of string
     *   from a 2-dim string array
     *   lecture 6 page 30
     */
    public List<List<String>> arrayToList(String[][] names) {
        List<List<String>> lists= new ArrayList<>();
        for (String[] name : names) {
            List<String> sublist = new ArrayList<>(Arrays.asList(name));
            lists.add(sublist);
        }
        return lists;
    }

    /*
     *   implement a function that return a list of prime factor of integer n
     *   in ascending order
     *   lecture 6 page 30
     */
    public List<Integer> primeFactors(int n) {
        List<Integer> primeFactors=new ArrayList<>();
        int primeNumber=2;
        while (n!=1){
            while (true){
                if(n%primeNumber==0){
                    if(!primeFactors.contains(primeNumber)){
                        primeFactors.add(primeNumber);
                    }
                    n/=primeNumber;
                }else{
                    break;
                }
            }
            while (true){
                primeNumber++;
                boolean flag=true;
                for(int i=2;i<primeNumber;i++){
                    if (primeNumber % i == 0) {
                        flag = false;
                        break;
                    }
                }
                if(flag){
                    break;
                }
            }
        }

        return primeFactors;
    }

    /*
     *   implement a function that return a list of words in a given string
     *   lecture 6 page 30
     */
    public List<String> extractWord(String line) {
        String[] split=line.split("\\W+");
        return new ArrayList<>(Arrays.asList(split));
    }
}
